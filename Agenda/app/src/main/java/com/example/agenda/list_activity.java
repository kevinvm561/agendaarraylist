package com.example.agenda;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.view.MenuInflater;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.Arrays;

public class list_activity extends AppCompatActivity {
    TableLayout tblLista;
    ArrayList<Contacto> contactos;

    SearchView srcList;
   ArrayList<Contacto> adapter;
    ArrayList<Contacto> arrayList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_activity);
        tblLista = (TableLayout)findViewById(R.id.tbLista);
        srcList=(SearchView)findViewById(R.id.menu_search);




        Bundle bundleObject = getIntent().getExtras();
        contactos = (ArrayList<Contacto>)bundleObject.getSerializable("contactos");
        this.adapter = this.contactos;
        Button btnNuevo = (Button)findViewById(R.id.btnNuevo);
        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        });
        cargarContactos();
    }
    public void buscar(String s){
        ArrayList<Contacto> list = new ArrayList<>();
        for(int x = 0; x < adapter.size(); x++)
        {
            if(adapter.get(x).getNombre().contains(s))
                list.add(adapter.get(x));
        }

        contactos = list;
        tblLista.removeAllViews();
        cargarContactos();
    }


    private void deleteContacto(int id)
    {
        for(int x = 0; x < adapter.size(); x++)
        {
            if(adapter.get(x).getID() == id)
            {
                Log.e("MSG", "Eliminado correctamente");
                this.adapter.remove(x);
                break;
            }
        }
        this.contactos = adapter;
        tblLista.removeAllViews();
        cargarContactos();
    }


    public View.OnClickListener btnEliminarAction(final int id)
    {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("ID", String.valueOf(id));
                deleteContacto(id);
            }
        };
    }

    public void cargarContactos() {
        for(int x=0; x < contactos.size(); x++) {
            Contacto c = contactos.get(x);
            TableRow nRow = new TableRow(list_activity.this);
            TextView nText = new TextView(list_activity.this);
            nText.setText(c.getNombre());

            nText.setTextSize(TypedValue.COMPLEX_UNIT_PT,6);
            nText.setTextColor((c.isFavorito()) ? Color.BLUE : Color.BLACK);
            nRow.addView(nText);

            Button nButton = new Button(list_activity.this);
            nButton.setText(R.string.accion);
            nButton.setTextSize(TypedValue.COMPLEX_UNIT_PT,6);
            nButton.setTextColor(Color.BLACK);

            nButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Contacto c= (Contacto) v.getTag(R.string.contacto_g);
                    Intent i = new Intent ();
                    Bundle oBundle = new Bundle();
                    oBundle.putSerializable("contactos", c);
                    oBundle.putSerializable("list", adapter);
                    oBundle.putInt(getString(R.string.accion), 1);
                    i.putExtras(oBundle);
                    setResult(RESULT_OK, i);
                    finish();
                }
            });
            nButton.setTag(R.string.contacto_g, c);

            Button btnEliminar = new Button(list_activity.this);
            btnEliminar.setText("Eliminar");
            btnEliminar.setTextSize(TypedValue.COMPLEX_UNIT_PT,6);
            btnEliminar.setTextColor(Color.BLACK);
            btnEliminar.setOnClickListener(this.btnEliminarAction(c.getID()));
            btnEliminar.setTag(R.string.contacto_g, c.getID());
            nRow.addView(btnEliminar);
            nRow.addView(nButton);
            tblLista.addView(nRow);
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK)
        {
            this.getLista();
        }
        return super.onKeyDown(keyCode, event);
    }

    private void getLista()
    {
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putSerializable("list", this.adapter);
        bundle.putInt(getString(R.string.accion), 0);
        intent.putExtras(bundle);
        setResult(Activity.RESULT_OK, intent);
        finish();
        Log.e("MSG", "Send Data");
    }

        public boolean onCreateOptionsMenu(Menu menu) {
            MenuInflater menuInflater = getMenuInflater();
            menuInflater.inflate(R.menu.searchview, menu);
            MenuItem menuItem = menu.findItem(R.id.menu_search);
            SearchView searchView = (SearchView) menuItem.getActionView();
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String s) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String s) {
                    buscar(s);
                    return false;
                }
            });
            return super.onCreateOptionsMenu(menu);
    }
}


