package com.example.agenda;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    final ArrayList<Contacto> contactos = new ArrayList<Contacto>();
    EditText editNombre;
    EditText editTelefono;
    EditText editTelefono2;
    EditText editDireccion;
    EditText editNotas;
    CheckBox chkFavorito;
    Contacto saveContact;
    Button btnSalir;

    private int saveIndex= 1;
    private boolean Editable=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editNombre = (EditText)findViewById(R.id.txtNombre);
        editTelefono = (EditText)findViewById(R.id.txtTel1);
        editTelefono2 = (EditText)findViewById(R.id.txtTel2);
        editDireccion = (EditText)findViewById(R.id.txtDomicilio);
        editNotas = (EditText)findViewById(R.id.txtNota);
        chkFavorito = (CheckBox)findViewById(R.id.chkFavorito);
        Button btnGuardar = (Button)findViewById(R.id.btnGuardar);
        Button btnLimpiar = (Button)findViewById(R.id.btnLimpiar);
        Button btnListar = (Button)findViewById(R.id.btnListar);
        btnSalir=(Button)findViewById(R.id.btnSalir);

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(editNombre.getText().toString().equals("") || editDireccion.getText().toString().equals("") || editTelefono.getText().toString().equals("")){
                    Toast.makeText(MainActivity.this, R.string.msgerror, Toast.LENGTH_SHORT).show();
                }else{
                    Contacto nContacto = new Contacto();


                    nContacto.setNombre(editNombre.getText().toString());
                    nContacto.setTelefono1(editTelefono.getText().toString());
                    nContacto.setTelefono2(editTelefono2.getText().toString());
                    nContacto.setDomicilio(editDireccion.getText().toString());
                    nContacto.setNotas(editNotas.getText().toString());
                    nContacto.setFavorito(chkFavorito.isChecked());


                    if(!Editable)
                    {
                        nContacto.setID(saveIndex);
                        contactos.add(nContacto);
                        saveIndex++;
                    }
                    else{
                        nContacto.setID(saveContact.getID());
                        editContacto(nContacto);
                    }


                    Toast.makeText(MainActivity.this, R.string.mensaje, Toast.LENGTH_SHORT).show();
                    limpiar();
                }
            }
        });

        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
                Intent i = new Intent(MainActivity.this, list_activity.class);
                Bundle bObject = new Bundle();
                bObject.putSerializable("contactos", contactos);
                i.putExtras(bObject);
                startActivityForResult(i,0);
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }


    private void editContacto(Contacto contacto)
    {
        for(int x = 0; x < contactos.size(); x++)
        {
            if(contactos.get(x).getID() == contacto.getID())
            {
                this.contactos.set(x, contacto);
                break;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(data != null) {
            Bundle oBundle = data.getExtras();
            int typeAction = oBundle.getInt(getString(R.string.accion));
            this.contactos.clear();
            this.contactos.addAll((ArrayList<Contacto>) oBundle.getSerializable("list"));
            if(typeAction == 1)
            {
                saveContact = (Contacto) oBundle.getSerializable("contactos");
                editNombre.setText(saveContact.getNombre());
                editTelefono.setText(saveContact.getTelefono1());
                editTelefono2.setText(saveContact.getTelefono2());
                editDireccion.setText(saveContact.getDomicilio());
                editNotas.setText(saveContact.getNotas());
                chkFavorito.setChecked(saveContact.isFavorito());
                Editable= true;
            }
        } else {
            limpiar();
        }
    }

    public void limpiar(){
        editNombre.setText("");
        editTelefono.setText("");
        editTelefono2.setText("");
        editDireccion.setText("");
        editNotas.setText("");
        chkFavorito.setChecked(false);
    }


}
